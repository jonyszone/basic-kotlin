# Kotlin Basic

## practicing topics

- Kotlin - Basic Types
- Control Flow
- Class & Object
- Constructors
- Inheritance
- Interface
- Visibility Control
- Extension
- Data Classes
- Sealed Class
- Generics
- Delegation
- Functions
- Destructuring Declarations
- Exception Handling

