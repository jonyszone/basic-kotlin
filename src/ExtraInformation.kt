class ExtraInformation : StudentInformation() {
    override val studentInfo: String
        get() = "Student's Extra Information"
}